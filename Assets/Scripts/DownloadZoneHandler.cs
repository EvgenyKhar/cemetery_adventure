﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownloadZoneHandler : MonoBehaviour
{
    [SerializeField] Canvas hintCanvas;
    LevelLoadHandler levelLoadHandler;
    Ammo player;
    private float hintTime = 3f;

    void Start()
    {
        StartCoroutine(ShowHint());
        levelLoadHandler = FindObjectOfType<LevelLoadHandler>();
    }

    IEnumerator ShowHint()
    {
        hintCanvas.enabled = true;
        yield return new WaitForSeconds(hintTime);
        hintCanvas.enabled = false;
    }

    void OnTriggerEnter()
    {
        //todo сделать перенос здоровья и снаряжения на следующий уровень
        player = FindObjectOfType<Ammo>();
        player.playerGeneration++;

        StartCoroutine(ShowHint());
        levelLoadHandler.LoadNextScene();
    }
}
