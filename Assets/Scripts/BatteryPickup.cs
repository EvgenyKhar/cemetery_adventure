﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickup : MonoBehaviour
{
    [SerializeField] float chargePercent=50f;
    

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerHealthSystem>())
        {
            FindObjectOfType<FlashLight>().RestoreLight(chargePercent);
            Destroy(gameObject);
        }
    }
}
