﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonHealthSystem : EnemyHealthSystem
{
    bool isInRage = false;
    [SerializeField] ParticleSystem[] rageVFX;

    void Update()
    {
        if(hp<=500 && !isInRage)
        {
            BecomesInRage();
        }
    }

    private void BecomesInRage()
    {
        isInRage = true;
        GetComponent<Animator>().SetTrigger("rage");
        foreach (ParticleSystem fx in rageVFX)
        {
            fx.Play();
        }
        print("becomes to rage");
        attackCollider.gameObject.transform.localScale = new Vector3(.5f, 2f, 2f);
    }
}
