﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthSystem : MonoBehaviour
{
    
    [SerializeField] float hp = 100;
    [SerializeField] Image damageImage;
    [SerializeField] float damagevisibleTime = 2f;
    [SerializeField] float disappearSpeed=0.1f;
    Color tempColor;
    bool isTrapped = false;
    Vector3 trappedPosition;

    void Start()
    {
        tempColor = damageImage.color;
        tempColor.a = 0f;
        damageImage.color = tempColor;
    }

    void Update()
    {
        if (isTrapped)
        {
            gameObject.transform.position = trappedPosition;
        }
        ReduceSplashVizible();
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;
        VisualizeDamage();
        
        if (hp <= 0)
        {
            StartDeathSequence();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        EnemyAttack enemyAttack = other.GetComponent<EnemyAttack>();
        if (enemyAttack)
        {
            hp -= enemyAttack.damage;
            VisualizeDamage();
        }

        if (hp <= 0)
        {
            StartDeathSequence();
        }
    }

    private void StartDeathSequence()
    {
        GetComponent<DeathHandler>().HandleDeath();
        FindObjectOfType<WeaponSwitcher>().enabled = false;
    }

    void VisualizeDamage()
    {

        tempColor = damageImage.color;
        tempColor.a = 1f;
        damageImage.color = tempColor;
    }

    void ReduceSplashVizible()
    {
        if (damageImage.color.a > 0)
        {
            tempColor = damageImage.color;
            tempColor.a -= disappearSpeed;
            damageImage.color = tempColor;
        }
    }

    public void Trap(bool swithcOn)
    {
        trappedPosition = gameObject.transform.position;
        isTrapped = swithcOn;
    }
}
