﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknestRaiser : MonoBehaviour
{
    [SerializeField] Light churchLight;
    [SerializeField] Light sun;
    [SerializeField] float decreasingSpeed = 0.1f;
    [SerializeField] float minimumIntensity = 0.2f;
    
    void Update()
    {

        if(churchLight.intensity> minimumIntensity)
        {
            churchLight.intensity -= decreasingSpeed * Time.deltaTime;
        }

        if(sun.intensity> minimumIntensity)
        {
            sun.intensity -= decreasingSpeed * Time.deltaTime;
        }
        
    }
}
