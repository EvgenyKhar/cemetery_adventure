﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo : MonoBehaviour
{
    
    [System.Serializable]
    public class AmmoSlot
    {
        public AmmoType ammoType;
        public int ammoAmount;
    }

    [SerializeField] public AmmoSlot[] ammoSlots = new AmmoSlot[4];
    Dictionary<AmmoType, AmmoSlot> ammoDict = new Dictionary<AmmoType, AmmoSlot>();
    public int playerGeneration = 0;

    void Start()
    {
        
        
        for (int i = 0; i < ammoSlots.Length; i++)
        {
            ammoDict.Add((AmmoType)i, ammoSlots[i]);
        }
    }


    public int GetCurrentAmmo(AmmoType ammoType)
    {
        return ammoDict[ammoType].ammoAmount;
    }

    public void ReduceAmmo(AmmoType ammoType)
    {
        ammoDict[ammoType].ammoAmount--;
    }

    public void IncreaseAmmo(AmmoType ammoType, int ammoAmount)
    {
        ammoDict[ammoType].ammoAmount += ammoAmount;
    }
}
