﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class WeaponSwitcher : MonoBehaviour
{
    [SerializeField] int curWeaponIndex = 0;
    [SerializeField] Transform[] weapons;
    int oldWeaponIndex;

    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        SetWeaponActive(curWeaponIndex);
    }
    void Update()
    {
        processKeyInput();
        ProcessScrollWheel();
    }

    private void processKeyInput()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetWeaponActive(0);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetWeaponActive(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SetWeaponActive(2);
        }
    }

    private void ProcessScrollWheel()
    {
        if(Input.GetAxis("Mouse ScrollWheel")>0)
        {
            if(curWeaponIndex>=transform.childCount-1)
            {
                SetWeaponActive(0);
            }
            else
            {
                SetWeaponActive(curWeaponIndex + 1);
            }
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (curWeaponIndex <=0)
            {
                SetWeaponActive(transform.childCount-1);
            }
            else
            {
                SetWeaponActive(curWeaponIndex - 1);
            }
        }
    }

    private void SetWeaponActive(int newWeaponIndex)
    {
        oldWeaponIndex = curWeaponIndex;
        curWeaponIndex = newWeaponIndex;
        transform.GetChild(oldWeaponIndex).gameObject.SetActive(false);
        transform.GetChild(curWeaponIndex).gameObject.SetActive(true);
    }


}
