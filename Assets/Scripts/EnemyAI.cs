﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;

public class EnemyAI : MonoBehaviour
{
    
    [SerializeField] float chaseRange = 5f;
    [SerializeField] GameObject attackCollider;
    [SerializeField] float turnSpeed = 5f;
    [SerializeField] AudioClip agroSFX;
    [SerializeField] AudioClip attackSFX;
    [SerializeField] AudioClip deathSFX;

    AudioSource audioSource;
    NavMeshAgent navMeshAgent;
    Animator enemyAnimator;
    Transform target;

    float angleBetween;


    float distanceToTarget = Mathf.Infinity;
    bool isProvoked = false;
    bool isAlive = true;
    public bool isAttacking = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        target = FindObjectOfType<PlayerHealthSystem>().transform;

        enemyAnimator = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        
        if(!isAlive)
        {
            navMeshAgent.enabled = false;
            enabled = false;
            return;
        }



        distanceToTarget = Vector3.Distance(target.position, gameObject.transform.position);

            if (isProvoked)
            {
                EngageTarget();
            }
            else if ( distanceToTarget - chaseRange < 0.1f)
            {
                isProvoked = true;
                audioSource.PlayOneShot(agroSFX);
            }
        

    }

    void EngageTarget()
    {
        if (!isAttacking)//makes for dodge avaliableness
        {
            FaceTarget();
        }

        if ( distanceToTarget - navMeshAgent.stoppingDistance > 0.1f && isAttacking==false)
        {
            ChaseTarget();
            
        }
        else
        {
            AttackTarget();
        }
    }



    void ChaseTarget()
    {
        isAttacking = false;
        navMeshAgent.enabled = true;
        attackCollider.GetComponent<Animator>().SetBool("attack", false);
        enemyAnimator.SetBool("attack", false);
        enemyAnimator.SetTrigger("move");
        navMeshAgent.SetDestination(target.position);

    }

    void AttackTarget()
    {
        navMeshAgent.enabled = false;
        attackCollider.GetComponent<Animator>().SetBool("attack", true);
        enemyAnimator.SetBool("attack", true);
    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, chaseRange);
    }

    public void OnDamageTaken()
    {
        if(!isProvoked)
        {
            isProvoked = true;
            audioSource.PlayOneShot(agroSFX);
        }
    }

    private void FaceTarget()
    {
            Vector3 direction = (target.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);   
    }

    public void SetToDead()
    {   
        isAlive = false;
        audioSource.PlayOneShot(deathSFX);
        attackCollider.GetComponent<Animator>().SetTrigger("dead");
    }

    public void PlayAttackSFX()
    {
        audioSource.PlayOneShot(attackSFX);
    }

}
