﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

public class WeaponZoom : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] float zoomedFOV = 20f;
    [SerializeField] float normalFOV = 60f;
    [SerializeField] float xZoomedSens = 0.5f;
    [SerializeField] float yZoomedSens = 0.5f;
    float xNormSens;
    float yNormSens;
    [SerializeField] MouseLook mouseLook;
    void Start()
    {
        mouseLook = GetComponentInParent<RigidbodyFirstPersonController>().mouseLook;
        xNormSens = mouseLook.XSensitivity;
        yNormSens = mouseLook.YSensitivity;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            mainCamera.fieldOfView = zoomedFOV;
            ChangeMouseSens(xZoomedSens, yZoomedSens);
        }
        else if(Input.GetMouseButtonUp(1))
        {
            mainCamera.fieldOfView = normalFOV;
            ChangeMouseSens(xNormSens, yNormSens);
        }
    }

    private void ChangeMouseSens(float xSensitivity, float ySensitivity)
    {
        mouseLook.XSensitivity = xSensitivity;
        mouseLook.YSensitivity = ySensitivity;
    }

    void OnDisable()
    {
        mainCamera.fieldOfView = normalFOV;
        ChangeMouseSens(xNormSens, yNormSens);
    }
}
