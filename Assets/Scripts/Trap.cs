﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    AudioSource audioSource;
    PlayerHealthSystem player;
    [SerializeField] float trapTime=5f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        player = FindObjectOfType<PlayerHealthSystem>();
    }

    void OnTriggerEnter()
    {
        audioSource.Play();
        player.Trap(true);
        StartCoroutine(Untrapp());

    }

    IEnumerator Untrapp()
    {
        yield return new WaitForSeconds(trapTime);
        player.Trap(false);
        Destroy(gameObject);
    }

}
