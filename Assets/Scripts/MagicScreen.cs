﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicScreen : MonoBehaviour
{
    [SerializeField] EnemyHealthSystem demon;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (demon.isDead)
        {
            Destroy(gameObject);
        }
    }
}
