﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;


public class Weapon : MonoBehaviour
{
    [SerializeField] Camera FPCamera;
    [SerializeField] float range=100f;
    [SerializeField] int hpPerHit = 1;
    [SerializeField] private ParticleSystem[] muzzleFlash;
    [SerializeField] GameObject hitEffect;
    [SerializeField] protected Ammo ammoSlots;
    [SerializeField] protected AmmoType ammoType;
    [Tooltip("in ms")][SerializeField] protected float shootingPeriod = 0.2f;
    protected bool isReloaded = true;
    Text ammoText;
    AudioSource shootSFX;

    void Start()
    {
        shootSFX = GetComponent<AudioSource>();
        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
    }

    void OnEnable()
    {
        if(!isReloaded)
        {
            StartCoroutine(ReloadBullet());
        }
    }

    void Update()
    {
        UpdateAmmoText();
        if (CrossPlatformInputManager.GetButton("Fire1"))
        {
           ProcessShooting();
        }
    }

    private void ProcessShooting()
    {
        if (ammoSlots.GetCurrentAmmo(ammoType) > 0 && isReloaded)
        {
            Shoot();
            StartCoroutine(ReloadBullet());
        }
    }

    protected IEnumerator ReloadBullet()
    {
        yield return new WaitForSeconds(shootingPeriod);
        isReloaded = true;
    }

    void Shoot()
    {
        shootSFX.Play();
        ammoSlots.ReduceAmmo(ammoType);
        isReloaded = false;
        PlayMuzzleFlash();
        try { ProcessRaycast();}
        catch
        {
            Debug.Log("While Raycasting from " + FPCamera.name + " something wents wrong");
        }
    }

    protected virtual void PlayMuzzleFlash()
    {
        foreach (ParticleSystem fx in muzzleFlash)
        {
            fx.Play();
        }
    }

    private void ProcessRaycast()
    {
        RaycastHit hit;
        if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
        {
            CreateHitImpact(hit);
            Transform hittenObject = hit.transform;
            if (hittenObject.GetComponent<EnemyHealthSystem>())
            {
                hittenObject.GetComponent<EnemyHealthSystem>().TakeDamage(this.hpPerHit);
            }
        }
    }

    private void CreateHitImpact(RaycastHit hit)
    {
        
        GameObject hitFx = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal), gameObject.transform);
        Destroy(hitFx, hitFx.GetComponent<ParticleSystem>().main.duration);
    }

    public void UpdateAmmoText()
    {
        if(ammoText==null)
        {
            ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
        }
        ammoText.text = ammoSlots.GetCurrentAmmo(ammoType).ToString();
    }
}
