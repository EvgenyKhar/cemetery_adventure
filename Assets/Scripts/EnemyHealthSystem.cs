﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthSystem : MonoBehaviour
{
    [SerializeField] protected int hp = 10;
    [SerializeField] protected GameObject attackCollider;

    public bool isDead = false;
    public void TakeDamage(int damageHP)
    {
        this.hp -= damageHP;
        BroadcastMessage("OnDamageTaken");
        

        if (this.hp<=0 && !isDead)
        {
            StartDeathSequence();
        }
    }

    private void StartDeathSequence()
    {
        GetComponent<EnemyAI>().SetToDead();
        GetComponent<Animator>().SetTrigger("die");
        attackCollider.SetActive(false);
        isDead = true;
    }



}
