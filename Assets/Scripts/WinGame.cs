﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGame : MonoBehaviour
{
    [SerializeField] Canvas winCanvas;
    [SerializeField] float textReadingTime = 10f;
    void Start()
    {
        winCanvas.enabled = false;
    }
    void OnTriggerEnter(Collider other)
    {
        winCanvas.enabled = true;
        StartCoroutine(LoadMainMenu());
    }

    IEnumerator LoadMainMenu()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        yield return new WaitForSeconds(textReadingTime);
        FindObjectOfType<LevelLoadHandler>().ToMainMenu();
    }

}
