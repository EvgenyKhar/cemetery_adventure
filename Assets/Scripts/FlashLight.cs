﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    [SerializeField] float lightDecay = 0.1f;
    [SerializeField] float angleDecay = 1f;
    [SerializeField] float minAngle = 30f;
    [SerializeField] float maxIntensity = 10f;
    [SerializeField] float maxAngle = 70f;

    bool isOn = true;
    Light myLight;

    void Start()
    {
        myLight = GetComponent<Light>();
    }

    void Update()
    {
        ProcessSwitching();
        if (isOn)
        {
            DecreaseLightAngle();
            DecreaseLightIntesity();
        }
    }

    private void ProcessSwitching()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            isOn = !isOn;
            myLight.enabled = !myLight.enabled;
        }
    }

    private void DecreaseLightAngle()
    {
        if(myLight.spotAngle>minAngle)
        {
            myLight.spotAngle -= Time.deltaTime * angleDecay;
        }
    }

    private void DecreaseLightIntesity()
    {
        if (myLight.intensity > 0)
        {
            myLight.intensity -= Time.deltaTime * lightDecay;
        }
    }

    public void RestoreLight(float chargePercent)
    {
        myLight.intensity += maxIntensity*chargePercent/100;
        if(myLight.intensity>maxIntensity)
        {
            myLight.intensity = maxIntensity;
        }

        myLight.spotAngle += maxAngle*chargePercent;
        if(myLight.spotAngle>maxAngle)
        {
            myLight.spotAngle = maxAngle;
        }
    }
}
