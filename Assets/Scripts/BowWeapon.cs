﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class BowWeapon : Weapon
{
    [SerializeField] GameObject arrow;
    [SerializeField] ParticleSystem bowFlash;

    bool isArrowHere = true;
    protected override void PlayMuzzleFlash()
    {
        bowFlash.Play();
        arrow.SetActive(false);
        Invoke("ReloadArrow", shootingPeriod);
    }

    void OnDisable()
    {
        CancelInvoke();
    }

    void OnEnable()
    {
        Invoke("ReloadArrow", shootingPeriod);
        if (!isReloaded)
        {
            StartCoroutine(ReloadBullet());
        }
    }

    void ReloadArrow()
    {
        if (ammoSlots.GetCurrentAmmo(this.ammoType) > 0)
        {
            arrow.SetActive(true);
        }
    }



}
