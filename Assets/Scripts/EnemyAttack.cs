﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    PlayerHealthSystem target;
    [SerializeField] public float damage =40f;
    EnemyAI enemyAI;

    void Start()
    {
        enemyAI = GetComponentInParent<EnemyAI>();
    }

    public void IsAttacking()
    {
       enemyAI.isAttacking = !enemyAI.isAttacking;
        enemyAI.PlayAttackSFX();
    }

}
